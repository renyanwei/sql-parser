# sql-parser

#### 介绍
基于druid框架分离的SQL解析器，并不断新增适配国产数据库。

#### 适配情况
1. mysql-支持
1. oracle-支持
1. sqlserver-支持
1. postgres-支持
1. db2-支持
1. h2-支持
1. derby-支持
1. sqlite-支持
1. sybase-支持
1. kingbase-支持（2023-02-06新增）


