/*
 * Copyright 1999-2017 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package etl.utils.sql.parser.repository;

import etl.utils.sql.parser.ast.SQLDeclareItem;
import etl.utils.sql.parser.ast.SQLObject;
import etl.utils.sql.parser.ast.SQLOver;
import etl.utils.sql.parser.ast.SQLParameter;
import etl.utils.sql.parser.ast.expr.*;
import etl.utils.sql.parser.ast.statement.*;
import etl.utils.sql.parser.visitor.SQLASTVisitor;

import java.util.HashMap;
import java.util.Map;

import static etl.utils.sql.parser.repository.SchemaResolveVisitorFactory.*;

/**
 * Created by wenshao on 03/08/2017.
 */
public interface SchemaResolveVisitor extends SQLASTVisitor {

    boolean isEnabled(Option option);

    int getOptions();

    SchemaRepository getRepository();

    Context getContext();

    Context createContext(SQLObject object);

    void popContext();

    default boolean visit(SQLSelectStatement x) {
        resolve(this, x.getSelect());
        return false;
    }

    default boolean visit(SQLSelect x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLWithSubqueryClause x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLIfStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLCreateFunctionStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLExprTableSource x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLSelectQueryBlock x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLForeignKeyImpl x) {
        resolve(this, x);
        return false;
    }

    default boolean visit(SQLIdentifierExpr x) {
        SchemaResolveVisitorFactory.resolveIdent(this, x);
        return true;
    }

    default boolean visit(SQLPropertyExpr x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLBinaryOpExpr x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLAllColumnExpr x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLCreateTableStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLUpdateStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLDeleteStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLAlterTableStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLInsertStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLParameter x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLDeclareItem x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLOver x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLMethodInvokeExpr x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLUnionQuery x) {
        SchemaResolveVisitorFactory.resolveUnion(this, x);
        return false;
    }

    default boolean visit(SQLMergeStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLCreateProcedureStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLBlockStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLReplaceStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    default boolean visit(SQLCastExpr x) {
        x.getExpr()
                .accept(this);
        return true;
    }

    default boolean visit(SQLFetchStatement x) {
        SchemaResolveVisitorFactory.resolve(this, x);
        return false;
    }

    public static enum Option {
        ResolveAllColumn,
        ResolveIdentifierAlias,
        CheckColumnAmbiguous;

        public final int mask;

        private Option() {
            mask = (1 << ordinal());
        }

        public static int of(Option... options) {
            if (options == null) {
                return 0;
            }

            int value = 0;

            for (Option option : options) {
                value |= option.mask;
            }

            return value;
        }
    }

    static class Context {
        public final Context parent;
        public final SQLObject object;
        public final int level;
        protected Map<Long, SQLDeclareItem> declares;
        private SQLTableSource tableSource;
        private SQLTableSource from;
        private Map<Long, SQLTableSource> tableSourceMap;

        public Context(SQLObject object, Context parent) {
            this.object = object;
            this.parent = parent;
            this.level = parent == null
                    ? 0
                    : parent.level + 1;
        }

        public SQLTableSource getFrom() {
            return from;
        }

        public void setFrom(SQLTableSource from) {
            this.from = from;
        }

        public SQLTableSource getTableSource() {
            return tableSource;
        }

        public void setTableSource(SQLTableSource tableSource) {
            this.tableSource = tableSource;
        }

        public void addTableSource(long alias_hash, SQLTableSource tableSource) {
            if (tableSourceMap == null) {
                tableSourceMap = new HashMap<Long, SQLTableSource>();
            }

            tableSourceMap.put(alias_hash, tableSource);
        }

        protected void declare(SQLDeclareItem x) {
            if (declares == null) {
                declares = new HashMap<Long, SQLDeclareItem>();
            }
            declares.put(x.getName().nameHashCode64(), x);
        }

        protected SQLDeclareItem findDeclare(long nameHash) {
            if (declares == null) {
                return null;
            }
            return declares.get(nameHash);
        }

        protected SQLTableSource findTableSource(long nameHash) {
            SQLTableSource table = null;
            if (tableSourceMap != null) {
                table = tableSourceMap.get(nameHash);
            }

            return table;
        }

        protected SQLTableSource findTableSourceRecursive(long nameHash) {
            for (Context ctx = this; ctx != null; ctx = ctx.parent) {
                if (ctx.tableSourceMap != null) {
                    SQLTableSource table = ctx.tableSourceMap.get(nameHash);
                    if (table != null) {
                        return table;
                    }
                }
            }

            return null;
        }
    }
}
