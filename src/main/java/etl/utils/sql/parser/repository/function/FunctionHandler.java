package etl.utils.sql.parser.repository.function;

public interface FunctionHandler {
    String getSignature();
}
