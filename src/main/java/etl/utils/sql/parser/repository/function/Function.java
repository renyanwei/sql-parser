package etl.utils.sql.parser.repository.function;

public interface Function {
    FunctionType getType();

    FunctionHandler findHandler();

    FunctionHandler findHandler(String signature);
}
