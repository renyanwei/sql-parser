package etl.utils.sql.parser.ast.statement;

import etl.utils.sql.parser.ast.SQLExpr;
import etl.utils.sql.parser.ast.SQLName;
import etl.utils.sql.parser.ast.SQLStatement;
import etl.utils.sql.parser.ast.SQLStatementImpl;
import etl.utils.sql.parser.visitor.SQLASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class SQLForStatement extends SQLStatementImpl {
    protected SQLName index;
    protected SQLExpr range;

    protected List<SQLStatement> statements = new ArrayList<SQLStatement>();

    public SQLForStatement() {

    }

    public SQLName getIndex() {
        return index;
    }

    public void setIndex(SQLName index) {
        this.index = index;
    }

    public SQLExpr getRange() {
        return range;
    }

    public void setRange(SQLExpr range) {
        if (range != null) {
            range.setParent(this);
        }
        this.range = range;
    }

    public List<SQLStatement> getStatements() {
        return statements;
    }

    @Override
    protected void accept0(SQLASTVisitor v) {
        if (v.visit(this)) {
            acceptChild(v, index);
            acceptChild(v, range);
            acceptChild(v, statements);
        }
        v.endVisit(this);

    }
}
