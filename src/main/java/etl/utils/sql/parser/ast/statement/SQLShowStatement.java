package etl.utils.sql.parser.ast.statement;

import etl.utils.sql.parser.ast.SQLStatement;

public interface SQLShowStatement extends SQLStatement {
}
