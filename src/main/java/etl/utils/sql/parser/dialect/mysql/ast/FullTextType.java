package etl.utils.sql.parser.dialect.mysql.ast;

/**
 * @author lijun.cailj 2018/8/14
 */
public enum FullTextType {
    CHARFILTER,
    TOKENIZER,
    TOKENFILTER,
    ANALYZER,
    DICTIONARY
}
