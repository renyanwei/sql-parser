package etl.utils.sql.parser.dialect.kingbase.ast.expr;

import etl.utils.sql.parser.ast.SQLExpr;
import etl.utils.sql.parser.ast.SQLObject;
import etl.utils.sql.parser.ast.SQLReplaceable;
import etl.utils.sql.parser.dialect.kingbase.visitor.KingBaseASTVisitor;

import java.util.Collections;
import java.util.List;

public class KingBaseCidrExpr extends KingBaseExprImpl implements SQLReplaceable {

    private SQLExpr value;

    public KingBaseCidrExpr clone() {
        KingBaseCidrExpr x = new KingBaseCidrExpr();
        if (value != null) {
            x.setValue(value.clone());
        }
        return x;
    }

    public SQLExpr getValue() {
        return value;
    }

    public void setValue(SQLExpr value) {
        this.value = value;
    }

    @Override
    public void accept0(KingBaseASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, value);
        }
        visitor.endVisit(this);
    }

    @Override
    public boolean replace(SQLExpr expr, SQLExpr target) {
        if (this.value == expr) {
            setValue(target);
            return true;
        }

        return false;
    }

    public List<SQLObject> getChildren() {
        return Collections.singletonList(value);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        KingBaseCidrExpr other = (KingBaseCidrExpr) obj;
        if (value == null) {
            return other.value == null;
        } else return value.equals(other.value);
    }

}
