package etl.utils.sql.parser.dialect.blink.vsitor;

import etl.utils.sql.parser.visitor.SQLASTVisitor;
import etl.utils.sql.parser.dialect.blink.ast.BlinkCreateTableStatement;

public interface BlinkVisitor extends SQLASTVisitor {
    boolean visit(BlinkCreateTableStatement x);

    void endVisit(BlinkCreateTableStatement x);
}
