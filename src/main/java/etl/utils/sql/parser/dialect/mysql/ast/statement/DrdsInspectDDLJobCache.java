package etl.utils.sql.parser.dialect.mysql.ast.statement;

import etl.utils.sql.parser.ast.SQLStatement;
import etl.utils.sql.parser.dialect.mysql.visitor.MySqlASTVisitor;

/**
 * version 1.0
 * Author zzy
 * Date 2019-07-22 10:06
 */
public class DrdsInspectDDLJobCache extends MySqlStatementImpl implements SQLStatement {

    public void accept0(MySqlASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

}
