package etl.utils.sql.parser.dialect.kingbase.ast.stmt;

import etl.utils.sql.parser.DbType;
import etl.utils.sql.parser.ast.SQLName;
import etl.utils.sql.parser.ast.SQLStatementImpl;
import etl.utils.sql.parser.dialect.kingbase.visitor.KingBaseASTVisitor;
import etl.utils.sql.parser.visitor.SQLASTVisitor;

public class KingBaseConnectToStatement extends SQLStatementImpl implements KingBaseSQLStatement {
    private SQLName target;

    public KingBaseConnectToStatement() {
        super(DbType.kingbase);
    }

    protected void accept0(SQLASTVisitor visitor) {
        this.accept0((KingBaseASTVisitor) visitor);
    }

    @Override
    public void accept0(KingBaseASTVisitor v) {
        if (v.visit(this)) {
            acceptChild(v, target);
        }
        v.endVisit(this);
    }

    public SQLName getTarget() {
        return target;
    }

    public void setTarget(SQLName x) {
        if (x != null) {
            x.setParent(this);
        }
        this.target = x;
    }
}
