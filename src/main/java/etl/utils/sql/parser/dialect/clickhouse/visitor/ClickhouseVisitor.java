package etl.utils.sql.parser.dialect.clickhouse.visitor;

import etl.utils.sql.parser.visitor.SQLASTVisitor;
import etl.utils.sql.parser.dialect.clickhouse.ast.ClickhouseCreateTableStatement;

public interface ClickhouseVisitor extends SQLASTVisitor {
    default boolean visit(ClickhouseCreateTableStatement x) {
        return true;
    }

    default void endVisit(ClickhouseCreateTableStatement x) {
    }
}
