package etl.utils.sql.parser.dialect.kingbase.ast.expr;

import etl.utils.sql.parser.SQLUtils;
import etl.utils.sql.parser.ast.SQLExprImpl;
import etl.utils.sql.parser.dialect.kingbase.visitor.KingBaseASTVisitor;
import etl.utils.sql.parser.visitor.SQLASTVisitor;


public abstract class KingBaseExprImpl extends SQLExprImpl implements KingBaseExpr {

    @Override
    public abstract void accept0(KingBaseASTVisitor visitor);

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        accept0((KingBaseASTVisitor) visitor);
    }

    public String toString() {
        return SQLUtils.toPGString(this);
    }
}
