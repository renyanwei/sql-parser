package etl.utils.sql.parser.dialect.kingbase.parser;

import etl.utils.sql.parser.ast.SQLPartitionBy;
import etl.utils.sql.parser.ast.SQLPartitionByHash;
import etl.utils.sql.parser.ast.SQLPartitionByList;
import etl.utils.sql.parser.parser.*;

public class KingBaseCreateTableParser extends SQLCreateTableParser {

    public KingBaseCreateTableParser(Lexer lexer) {
        super(new KingBaseExprParser(lexer));
    }

    public KingBaseCreateTableParser(String sql) {
        super(new KingBaseExprParser(sql));
    }

    public KingBaseCreateTableParser(SQLExprParser exprParser) {
        super(exprParser);
    }

    public SQLPartitionBy parsePartitionBy() {
        lexer.nextToken();
        accept(Token.BY);

        if (lexer.identifierEquals("LIST")) {
            lexer.nextToken();
            SQLPartitionByList list = new SQLPartitionByList();

            if (lexer.token() == Token.LPAREN) {
                lexer.nextToken();
                list.addColumn(this.exprParser.expr());
                accept(Token.RPAREN);
            } else {
                acceptIdentifier("COLUMNS");
                accept(Token.LPAREN);
                for (; ; ) {
                    list.addColumn(this.exprParser.name());
                    if (lexer.token() == Token.COMMA) {
                        lexer.nextToken();
                        continue;
                    }
                    break;
                }
                accept(Token.RPAREN);
            }

            return list;
        } else if (lexer.identifierEquals("HASH") || lexer.identifierEquals("UNI_HASH")) {
            SQLPartitionByHash hash = new SQLPartitionByHash();

            if (lexer.identifierEquals("UNI_HASH")) {
                hash.setUnique(true);
            }

            lexer.nextToken();

            if (lexer.token() == Token.KEY) {
                lexer.nextToken();
                hash.setKey(true);
            }

            accept(Token.LPAREN);
            this.exprParser.exprList(hash.getColumns(), hash);
            accept(Token.RPAREN);
            return hash;
        }

        throw new ParserException("TODO " + lexer.info());
    }
}
