/*
 * Copyright 1999-2017 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package etl.utils.sql.parser.dialect.kingbase.ast.stmt;

import etl.utils.sql.parser.DbType;
import etl.utils.sql.parser.ast.statement.SQLSelect;
import etl.utils.sql.parser.ast.statement.SQLSelectStatement;
import etl.utils.sql.parser.dialect.kingbase.visitor.KingBaseASTVisitor;
import etl.utils.sql.parser.visitor.SQLASTVisitor;

public class KingBaseSelectStatement extends SQLSelectStatement implements KingBaseSQLStatement {

    public KingBaseSelectStatement() {
        super(DbType.kingbase);
    }

    public KingBaseSelectStatement(SQLSelect select) {
        super(select, DbType.kingbase);
    }

    protected void accept0(SQLASTVisitor visitor) {
        if (visitor instanceof KingBaseASTVisitor) {
            accept0((KingBaseASTVisitor) visitor);
        } else {
            super.accept0(visitor);
        }
    }

    public void accept0(KingBaseASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.select);
        }
        visitor.endVisit(this);
    }
}
