package etl.utils.sql.parser.dialect.ads.visitor;

import etl.utils.sql.parser.visitor.SQLASTVisitor;
import etl.utils.sql.parser.dialect.mysql.ast.MySqlPrimaryKey;
import etl.utils.sql.parser.dialect.mysql.ast.statement.MySqlCreateTableStatement;

public interface AdsVisitor extends SQLASTVisitor {
    boolean visit(MySqlPrimaryKey x);

    void endVisit(MySqlPrimaryKey x);

    boolean visit(MySqlCreateTableStatement x);

    void endVisit(MySqlCreateTableStatement x);
}
