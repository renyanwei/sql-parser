/*
 * Copyright 1999-2017 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package etl.utils.sql.parser.dialect.kingbase.visitor;

import etl.utils.sql.parser.ast.statement.SQLSelectQueryBlock;
import etl.utils.sql.parser.ast.statement.SQLSelectStatement;
import etl.utils.sql.parser.dialect.kingbase.ast.expr.*;
import etl.utils.sql.parser.dialect.kingbase.ast.stmt.*;
import etl.utils.sql.parser.visitor.SQLASTVisitor;

public interface KingBaseASTVisitor extends SQLASTVisitor {

    default void endVisit(KingBaseSelectQueryBlock x) {
        endVisit((SQLSelectQueryBlock) x);
    }

    default boolean visit(KingBaseSelectQueryBlock x) {
        return visit((SQLSelectQueryBlock) x);
    }

    default void endVisit(KingBaseSelectQueryBlock.FetchClause x) {
    }

    default boolean visit(KingBaseSelectQueryBlock.FetchClause x) {
        return true;
    }

    default void endVisit(KingBaseSelectQueryBlock.ForClause x) {
    }

    default boolean visit(KingBaseSelectQueryBlock.ForClause x) {
        return true;
    }

    default void endVisit(KingBaseDeleteStatement x) {
    }

    default boolean visit(KingBaseDeleteStatement x) {
        return true;
    }

    default void endVisit(KingBaseInsertStatement x) {
    }

    default boolean visit(KingBaseInsertStatement x) {
        return true;
    }

    default void endVisit(KingBaseSelectStatement x) {
        endVisit((SQLSelectStatement) x);
    }

    default boolean visit(KingBaseSelectStatement x) {
        return visit((SQLSelectStatement) x);
    }

    default void endVisit(KingBaseUpdateStatement x) {
    }

    default boolean visit(KingBaseUpdateStatement x) {
        return true;
    }

    default void endVisit(KingBaseFunctionTableSource x) {
    }

    default boolean visit(KingBaseFunctionTableSource x) {
        return true;
    }

    default void endVisit(KingBaseTypeCastExpr x) {
    }

    default boolean visit(KingBaseTypeCastExpr x) {
        return true;
    }

    default void endVisit(KingBaseExtractExpr x) {
    }

    default boolean visit(KingBaseExtractExpr x) {
        return true;
    }

    default void endVisit(KingBaseBoxExpr x) {
    }

    default boolean visit(KingBaseBoxExpr x) {
        return true;
    }

    default void endVisit(KingBasePointExpr x) {
    }

    default boolean visit(KingBasePointExpr x) {
        return true;
    }

    default void endVisit(KingBaseMacAddrExpr x) {
    }

    default boolean visit(KingBaseMacAddrExpr x) {
        return true;
    }

    default void endVisit(KingBaseInetExpr x) {
    }

    default boolean visit(KingBaseInetExpr x) {
        return true;
    }

    default void endVisit(KingBaseCidrExpr x) {
    }

    default boolean visit(KingBaseCidrExpr x) {
        return true;
    }

    default void endVisit(KingBasePolygonExpr x) {
    }

    default boolean visit(KingBasePolygonExpr x) {
        return true;
    }

    default void endVisit(KingBaseCircleExpr x) {
    }

    default boolean visit(KingBaseCircleExpr x) {
        return true;
    }

    default void endVisit(KingBaseLineSegmentsExpr x) {
    }

    default boolean visit(KingBaseLineSegmentsExpr x) {
        return true;
    }

    default void endVisit(KingBaseShowStatement x) {
    }

    default boolean visit(KingBaseShowStatement x) {
        return true;
    }

    default void endVisit(KingBaseStartTransactionStatement x) {
    }

    default boolean visit(KingBaseStartTransactionStatement x) {
        return true;
    }

    default void endVisit(KingBaseConnectToStatement x) {
    }

    default boolean visit(KingBaseConnectToStatement x) {
        return true;
    }

    default void endVisit(KingBaseCreateSchemaStatement x) {
    }

    default boolean visit(KingBaseCreateSchemaStatement x) {
        return true;
    }

    default void endVisit(KingBaseDropSchemaStatement x) {
    }

    default boolean visit(KingBaseDropSchemaStatement x) {
        return true;
    }

    default void endVisit(KingBaseAlterSchemaStatement x) {
    }

    default boolean visit(KingBaseAlterSchemaStatement x) {
        return true;
    }

}
