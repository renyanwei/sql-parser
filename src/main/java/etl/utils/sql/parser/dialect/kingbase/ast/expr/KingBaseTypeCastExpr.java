package etl.utils.sql.parser.dialect.kingbase.ast.expr;

import etl.utils.sql.parser.SQLUtils;
import etl.utils.sql.parser.ast.expr.SQLCastExpr;
import etl.utils.sql.parser.dialect.kingbase.visitor.KingBaseASTVisitor;
import etl.utils.sql.parser.visitor.SQLASTVisitor;

public class KingBaseTypeCastExpr extends SQLCastExpr implements KingBaseExpr {

    @Override
    public void accept0(KingBaseASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.expr);
            acceptChild(visitor, this.dataType);
        }
        visitor.endVisit(this);
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor instanceof KingBaseASTVisitor) {
            accept0((KingBaseASTVisitor) visitor);
            return;
        }

        super.accept0(visitor);
    }

    public String toString() {
        return SQLUtils.toPGString(this);
    }
}
