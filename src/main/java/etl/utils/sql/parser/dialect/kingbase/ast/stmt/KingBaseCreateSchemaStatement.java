/*
 * Copyright 1999-2017 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package etl.utils.sql.parser.dialect.kingbase.ast.stmt;

import etl.utils.sql.parser.ast.SQLStatementImpl;
import etl.utils.sql.parser.ast.expr.SQLIdentifierExpr;
import etl.utils.sql.parser.ast.statement.SQLCreateStatement;
import etl.utils.sql.parser.dialect.kingbase.visitor.KingBaseASTVisitor;
import etl.utils.sql.parser.visitor.SQLASTVisitor;

public class KingBaseCreateSchemaStatement extends SQLStatementImpl implements KingBaseSQLStatement, SQLCreateStatement {

    private SQLIdentifierExpr schemaName;
    private SQLIdentifierExpr userName;
    private boolean ifNotExists;
    private boolean authorization;

    public SQLIdentifierExpr getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(SQLIdentifierExpr schemaName) {
        this.schemaName = schemaName;
    }

    public SQLIdentifierExpr getUserName() {
        return userName;
    }

    public void setUserName(SQLIdentifierExpr userName) {
        this.userName = userName;
    }

    public boolean isIfNotExists() {
        return ifNotExists;
    }

    public void setIfNotExists(boolean ifNotExists) {
        this.ifNotExists = ifNotExists;
    }

    public boolean isAuthorization() {
        return authorization;
    }

    public void setAuthorization(boolean authorization) {
        this.authorization = authorization;
    }

    protected void accept0(SQLASTVisitor visitor) {
        accept0((KingBaseASTVisitor) visitor);
    }

    @Override
    public void accept0(KingBaseASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.schemaName);
            acceptChild(visitor, this.userName);
        }
        visitor.endVisit(this);
    }
}
