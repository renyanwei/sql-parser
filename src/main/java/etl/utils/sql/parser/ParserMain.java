package etl.utils.sql.parser;

import etl.utils.sql.parser.ast.SQLStatement;
import etl.utils.sql.parser.stat.TableStat;
import etl.utils.sql.parser.visitor.SchemaStatVisitor;

import java.util.List;
import java.util.Map;

public class ParserMain {

    public static void main(String[] args) {
        // new Test().testSqlParser();
    }

    public void testSqlParser() {
        String sql = "merge into t1 using t2 on  (t1.id = t2.id)  when matched then update set t1.id = 100;";
        //String result = SQLUtils.format(sql, dbType);
        List<SQLStatement> statementList = SQLUtils.parseStatements(sql,DbType.kingbase);
        for(int i = 0; i < statementList.size() ; i++){
            SQLStatement statement = statementList.get(i);
            SchemaStatVisitor schemaStatVisitor = SQLUtils.createSchemaStatVisitor(DbType.kingbase);
            statement.accept(schemaStatVisitor);
            Map<TableStat.Name, TableStat> tables = schemaStatVisitor.getTables();
            tables.forEach( (name,state) ->{
                System.out.println("name: " + name + " op: " + state.getSelectCount() );
            } );
        }
    }
}
